FROM ubuntu:20.04
ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install -y \
	python3.8 \
	git \
	nano \
	gcc \
	g++ \
    libpthread-stubs0-dev \
    zlib1g-dev \
    autotools-dev \
    asciidoctor \
    default-jre \
    wget \
    libidn11 \
    unzip

# Install IgBLAST executables
## Add method to include IMGT VDJ definitions
ENV IGBLAST_VERSION 1.6.1
RUN wget ftp://ftp.ncbi.nih.gov/blast/executables/igblast/release/$IGBLAST_VERSION/ncbi-igblast-$IGBLAST_VERSION-x64-linux.tar.gz

RUN tar zxf ncbi-igblast-$IGBLAST_VERSION-x64-linux.tar.gz \
    && rm ncbi-igblast-$IGBLAST_VERSION-x64-linux.tar.gz
ENV PATH /ncbi-igblast-$IGBLAST_VERSION/bin:$PATH

## Run igBLAST tests
RUN igblastn -help \
    && igblastp -help \
    && makeblastdb -help

# Get precompiled jar, unzip and test
RUN wget https://github.com/mikessh/migmap/releases/download/1.0.3/migmap-1.0.3.zip
RUN unzip migmap-1.0.3.zip \
    && rm migmap-1.0.3.zip

## Old not working method because migmap itself does not provide compiled binaries
#ARG DEBIAN_FRONTEND=noninteractive
#RUN apt-get update && apt-get install -y tzdata
#RUN unlink /etc/localtime
#RUN ln -s /usr/share/zoneinfo/Europe/Zurich /etc/localtime

#RUN apt-get update && apt-get install -y \
#	python3.8 \
#	git \
#	nano \
#	gcc \
#	g++ \
#    libpthread-stubs0-dev \
#    zlib1g-dev \
#    autotools-dev \
#    asciidoctor \
#    default-jre \
#    wget \
#    libidn11
    
# Install IgBLAST executables
## Add method to include IMGT VDJ definitions
#ENV IGBLAST_VERSION 1.6.1
#RUN wget ftp://ftp.ncbi.nih.gov/blast/executables/igblast/release/$IGBLAST_VERSION/ncbi-igblast-$IGBLAST_VERSION-x64-linux.tar.gz

#RUN tar zxf ncbi-igblast-$IGBLAST_VERSION-x64-linux.tar.gz
#ENV PATH /ncbi-igblast-$IGBLAST_VERSION/bin:$PATH

# Test igBLAST
#RUN igblastn -help \
#    && igblastp -help \
#    && makeblastdb -help
 
#RUN git clone https://bitbucket.org/jspagnuolo/migmap migmap
#RUN cd migmap

## Test migmap
#RUN java -jar migmap.jar -h
#RUN java -jar migmap.jar -S human -R IGH src/test/resources/sample.fasta.gz test

